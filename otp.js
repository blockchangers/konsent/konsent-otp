var speakeasy = require('speakeasy');

const generateSecret    = () => speakeasy.generateSecret({length: 20});
const generateToken     = (secret, counter) => speakeasy.hotp({secret, encoding: 'base32', counter});
const isValidToken      = (secret, token, counter) =>  speakeasy.totp.verify({secret,  encoding: 'base32', token, counter});

module.exports = {
  generateSecret,
  generateToken,
  isValidToken
};

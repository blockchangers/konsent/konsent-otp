const UserNotFound = require('./UserNotFound');
const JwtValidation = require('./JwtValidation');
const DatabaseError = require('./DatabaseError');
const InvalidArgument = require('./InvalidArgument');

module.exports = {
  UserNotFound,
  JwtValidation,
  DatabaseError,
  InvalidArgument
};

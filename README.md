# Konsent OTP

## Intro

Provides the otp generation for the signup step in konsent.

Validates generated token and signs a claim about phone number(jwt) which is passed back to the frontend client (konsent-mobile).

## Installation

```
npm i
npm run wallet:create
```


## Running on localhost
```
nvm use
npm install
npm run ngrok
```

## What to do when it doesnt work?
* Have you any credit left on Twilio ?
* Have you payed the Ngrok bill?
* Is the tunnel configured at Ngrok?

## Dependencies
### Providers

* Twilio (SMS gateway)
* Ngrok (Tunnel provider)


### Twilio
Twilio is configured with a webhook that posts the token to the API
```
https://konsent-otp.eu.ngrok.io/api/v1/otp/generate
```

this will generate a OTK/OTP (one time token/password) and pass it back to the user as an text message

### Ngrok
You can start the application and punsh a hole in network with ngrok.
The follow tunnel have been configured at ngrok

(https://konsent-otp.eu.ngrok.io)

When you start the application with it will point back to your local development machine:

```
npm run ngrok
```

## Deployment
to be continued
* Deploy digital ocean
* Point twilio webhook to correct URL

### Analytics
New Relic

### Logging

Loggly

### Metrics

New Relic


## TODO

#### Sms Gateway
* twilio is expensive , replace with cheaper one



### API documentation

N/A

### Tests
N/A


## https://www.websequencediagrams.com/
```
title Konsent OTP (signup)

User->Konsent Mobile : Enter signup
Konsent Mobile->User : Ask user to send SMS to numer X
User->Konsent OTP: sends SMS
Konsent OTP->Konsent OTP: generate token and store association between phone and token
Konsent OTP--> User: token
User->Konsent Mobile : input token and from number
Konsent Mobile -> Konsent OTP: validate association between phone and token
Konsent OTP-->Konsent Mobile : signed JWT claim (phone)
```

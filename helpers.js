const { Async, tryCatch }       = require('crocks');
const { readFile, writeFile }   = require('fs');

const readFileAsync             = Async.fromNode(readFile);
const writeFileAsync            = Async.fromNode(writeFile);
const parse                     = tryCatch(JSON.parse);
const createFormattedJSON       = obj => JSON.stringify(obj, null, 2);


module.exports = {
  parse,
  readFileAsync,
  writeFileAsync,
  createFormattedJSON
};


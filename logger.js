require('dotenv').config();

const winston = require('winston');

var {Loggly} = require('winston-loggly-bulk');

const LOGGLY_TOKEN = process.env.LOGGLY_TOKEN;

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;


const konsentFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const logger = createLogger({
  level: 'info',
  format: format.json(),
  transports: [
    new transports.File({ filename: './logs/error.log', level: 'error' }),
    new transports.File({ filename: './logs/combined.log' })
  ]
});

if (process.env.NODE_ENV === 'production') {
  logger.add(new Loggly({
    token: LOGGLY_TOKEN,
    subdomain: "blockchangers",
    tags: ["Winston-NodeJS"],
    json:true
  }));
}

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: combine(
      label({ label: 'konsent-otp' }),
      timestamp(),
      konsentFormat
    )
  }));
};

const messageLogger = (title, message) => {
  const wrapTitle = title ? ` \n ${title} \n ${'-'.repeat(60)}` : ''
  const wrapMessage = `\n ${'-'.repeat(60)} ${wrapTitle} \n`
  console.log(wrapMessage)
  console.log(message)
};

const logo = `
▒█████  ▄▄▄█████▓ ██▓███
▒██▒  ██▒▓  ██▒ ▓▒▓██░  ██▒
▒██░  ██▒▒ ▓██░ ▒░▓██░ ██▓▒
▒██   ██░░ ▓██▓ ░ ▒██▄█▓▒ ▒
░ ████▓▒░  ▒██▒ ░ ▒██▒ ░  ░
░ ▒░▒░▒░   ▒ ░░   ▒▓▒░ ░  ░
  ░ ▒ ▒░     ░    ░▒ ░
░ ░ ░ ▒    ░      ░░
    ░ ░
`;

module.exports = {
  logo,
  logger,
  messageLogger
};

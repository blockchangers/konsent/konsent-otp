require('dotenv').config();
require('newrelic');

const http                                            = require('http');
const express                                         = require('express');
const morgan                                          = require('morgan');
const cors                                            = require('cors');
const bodyParser                                      = require('body-parser');
const compression                                     = require('compression')
const levelup                                         = require('levelup');
const leveldown                                       = require('leveldown');
const MessagingResponse                               = require('twilio').twiml.MessagingResponse;
const { generateToken, isValidToken, generateSecret } = require('./otp');
const { getSigner }                                   = require('./did');
const { logger, logo }                                = require('./logger');
const repository                                      = require('./db/repository');

const app = express();
app.server = http.createServer(app);

app.use(compression())
app.use(cors());
app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

(async () => {
  try {
    const signer = await getSigner();

    const DB_PATH = process.env.DB_PATH || './users';
    const store = levelup(leveldown(DB_PATH));

    // webhook from twilio , should never be called directly
    app.post('/api/v1/otp/generate', async (req, res) => {
      try {
        const {From} = req.body;
        const phone = From;

        if(!phone) {
          logger.error('malformed request, missing phone number');
          return res.status(400).send();
        }

        const secret = generateSecret();
        const counter = new Date().getTime();
        const token = generateToken(secret.base32, counter);

        logger.info(`received generate token request from: ${phone}`);

        await repository.setUser(store, phone, { token, secret: secret.base32, counter}); // TODO: Remove, and store in memory

        const twiml = new MessagingResponse();
        twiml.message(token);
        res.writeHead(200, { 'Content-Type': 'text/xml' });
        res.end(twiml.toString());
      } catch (error) {
        logger.error(error);
        if (error instanceof DatabaseError) {
          return res.status(404).send({ message: 'database problems', error: error.stack });
        } else {
          return res.status(500).send({ message: 'internal server error', error: error.stack });
        }
      }
    });

    // validate token from the mobile app and send back signed claim about phone number
    app.post('/api/v1/otp/validate', async (req, res) => {
      try {
        const token = req.body.token;
        const phone = req.body.phone;

        if(!phone) {
          logger.error('malformed request, missing phone number');
          return res.status(400).send();
        }

        if(!token) {
          logger.error('malformed request, missing token');
          return res.status(400).send();
        }

        const user = await repository.getUser(store, phone);
        if(!user) {
          logger.error('cant find user: ', phone);
          return res.status(404).send();
        }

        if(user.token !== token) {
          logger.error('non matching token', token);
          return res.status(400).send();
        }

        logger.info(`trying to validate token from source : ${phone}`);

        if(isValidToken(user.secret, token, user.counter)) {
          try {
            await repository.deleteUser(store, phone);
            logger.info(`validated token: ${token} from source : ${phone}`);
            const jwt = await signer.signJWT({claims: [{type: 'phone', value: phone}]});
            res.json({jwt});
          } catch (error) {
            logger.error(error);
            res.status(400).send();
          }

        } else {
          logger.error(`cant validate token : ${token} from source: ${phone}`);
          res.status(400).send();
        }
      } catch (error) {
        logger.error(error);
        if (error instanceof DatabaseError) {
          return res.status(404).send({ message: 'database problems', error: error.stack });
        } else {
          return res.status(500).send({ message: 'internal server error', error: error.stack });
        }
      }
    });

    app.use((err, req, res, next) => {
      logger.error(err.stack)
      res.status(500).send('internal server error, report to support@blockchangers.com');
    });

    const server = app.listen(process.env.PORT || 8888, (port) => {
      console.log(`otp with twilio webhook callbak up and running at port: ${process.env.PORT || 8888}`)
      console.log(logo);
      app._router.stack.forEach(function(r){
        if (r.route && r.route.path){
          console.log(r.route.path)
        }
      });
    });
  } catch (error) {
    console.error(error);
    logger.error(error);
    process.exit(1);
  }
})();

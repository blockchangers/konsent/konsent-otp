const R = require('ramda');

const getUser = R.curry(async(db, ssn) => {
  return await db.get(ssn);
});

const setUser = R.curry(async(db, ssn, obj) => {
  return await db.put(ssn, JSON.stringify(obj));
});

const deleteUser = R.curry(async(db, ssn) => {
  return await db.del(ssn);
});

module.exports = {
  getUser,
  setUser,
  deleteUser
};


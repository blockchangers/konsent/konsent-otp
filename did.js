require('dotenv').config();

const EthrDID   = require('ethr-did');
const wallet    = require('./wallet');
const web3      = require('./web3');

const PATH_TO_EXPORTED_WALLET       = process.env.PATH_TO_EXPORTED_WALLET;
const WALLET_PASSWORD               = process.env.WALLET_PASSWORD;

const getSigner = async function() {
  const loadedWallet = await wallet.load(PATH_TO_EXPORTED_WALLET, WALLET_PASSWORD);

  const signer = new EthrDID({
    address: loadedWallet.address,
    privateKey: loadedWallet.privateKey.substring(2),
    provider: web3.currentProvider
  });

  return signer;
};

module.exports = {
  getSigner
};
